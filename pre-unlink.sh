if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ApplyXFM_gui ConcatXFM_gui Flirt_gui InvertXFM_gui Nudge_gui aff2rigid applyxfm4D avscale convert_xfm epi_reg extracttxt flirt flirt_average img2imgcoord img2stdcoord makerot midtrans pairreg pointflirt rmsdiff standard_space_roi std2imgcoord
fi
